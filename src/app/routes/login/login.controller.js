define('login.controller',['angular','auth.service'],
	function(){
		'use strict';

		angular.module('loginControllerModule', ['authServiceModule'])
			.controller('loginController',[
				'authService',
				'$scope',
				function loginController(authService, scope){

					var that = this;

					/** @function
					 * The login function
					 * @name login */
					function login(){

						/*jshint validthis:true*/							
						var authData = {
							email: that.email,
							password: that.password,
							rememberMe: that.rememberMe
						};

						that.submitDisabled = true;

						authService.login(authData)
							.then(function onLoginSuccess(){
								scope.$root.state.transitionTo('resources.list');
							})
							.catch(function onLoginError(responseData){
								that.errorMessage = responseData.message;
							})
							.finally(function onLoginFinal(){
								that.submitDisabled = false;
							});				
					}

					angular.extend(that, {
						login: login
					});
				}
			])
			.controller('logoutController',[
				'authService',
				'$scope',
				function logoutController(authService, scope){
					var that = this;

					function logout(){
						authService.logout()
							.then(function onLogoutSuccess(){
								scope.$root.state.transitionTo('login');
							});				
					}
					logout();
				}
			]);
	}
);