define('login.route',['angular','login.controller'],
	function(){
		'use strict';

		angular.module('loginRouteModule',['loginControllerModule']);

		return function loginRouter($stateProvider) {
			$stateProvider
				.state('login', {
					url: '/login',
					templateUrl: 'app/routes/login/login.template.html',
					controller: 'loginController as ctrl',
					title:'Login'
				})
				.state('logout', {
					url: '/logout',
					//templateUrl: 'app/routes/login/login.template.html',
					controller: 'logoutController as ctrl'
					//title:'Login'
				});
    	};
	}
);
