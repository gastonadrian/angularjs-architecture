define('auth.service',['angular','angularResource','configuration','response-transformer','session-storage'],
	function authenticationService(ng, angularResource, configuration, responseTransformer){
		'use strict';

		angular.module('authServiceModule', ['ngResource','sessionStorageModule'])
			.factory('authDataContext', [
				'$resource',
				'$q',
				function authDataContext($resource, $q){

					var resources = $resource(configuration.serviceBaseUrl + '/:controller/:action', {  }, {
                login: { method: 'POST', params: { controller: 'login' }, headers:{ username:'@email', password:'@password' }, isArray: false, timeout: 60000 }
          });

					/** @function
					 * Logs in against a restful service, the service returns an object, in case status != 'ok', the object cames with a 'message' property
					 * explaining why the log in failed
					 * @name login
					 * @param {object} data -authentication data { email : {string}, password: {string}, rememberMe: {boolean} };
					 * @summary Performs log in against the server */
                    function login(data){
                    	var promise = resources.login(data);
                    	return promise
                    		.$promise
							/**
							* @argument {object} responseData  -authentication response { result : [{ status: 'ok'|'error', message: {string}, access_token: {string} }] };
							*/
                    		.then(function onLoginResponse(responseData){
	                			responseData = responseData.result && responseData.result.length && responseData.result[0];

	                			var data = {
	                				disabledItems:[],
	                				token: null
	                			};

	                			if(responseData && responseData.status === 'ok'){
	                    			data.disabledItems = [];
	                    			/*jshint camelcase:false*/
	                    			data.token = responseData['access-token'];
									/*jshint camelcase:true*/
	                			}
	                			else{
	                				return $q.reject(responseData);
	                			}

	                			return data;
	                    	})
	                    	.catch(function onLoginError(responseData){
	                    		responseData.message = responseData.message || 'An error has ocurred, please try again';
	                    		return $q.reject(responseData);
	                    	});
                    }

                    /*jshint unused:false */
                    function mockLogin(){
											var promise = $q.defer();

											promise.resolve({
					            				disabledItems:[],
					            				token: 'ZLtdOEj3vRc='
					            			});

											promise.$promise = promise.promise;

											return promise.promise;
                    }
                    /*jshint unused:true */

					return {
						login: login //mockLogin,
					};
				}
			])
			.factory('authService',[
				'authDataContext',
				'$window',
				'$q',
				'sessionStorage',
				function authService(dc, $window, $q, session){

					var authData = {
						token: null,
						menuItems: configuration.menuItems,
						user: {},
						newUser: false
					},
					menuItemsPromise = $q.defer();

					/** @function
					 * @name login */
					function login(data){

						var result = dc.login(data);

						return result
							.then(function onLoginOk(response){
								authData.token = response.token;

								authData.newUser = !!response.newUser;
								authData.user = response.user || {};

								// store the username to ask later for user settings
								authData.user.username = data.email;

								disableMenuItems(response.disabledItems);

								// place token on configuration to avoid dependency of auth service on http injector
								configuration.token = authData.token;
								session.setItem('access-token', authData.token, configuration.sessionTimeoutMinutes || 30);

								return response;
							})
							.catch(function onLoginError(response){
								authData.token = null;
								session.removeItem('access-token');
								session.removeItem('tenantId');
								return $q.reject(response);
							});
					}

					/** @function
					 * @name logout */
					function logout(data){
						var promise = $q.defer();

						session.removeItem('access-token');
						configuration.token = null;

						promise.resolve({});

						return promise.promise;
					}

					/** @function
					 * Disable Menu Entries for the current user and notifies the subscribers
					 * @name disableMenuItems
					 * @param {Array} disabledItems -array of string with the name of the menu items to disable */
					function disableMenuItems(disabledItems){
						disabledItems = disabledItems || [];

						angular.forEach(authData.menuItems, function menuItemsEach(el){
							el.disabled = false;
						});

						for (var i = 0; i < disabledItems.length; i++) {
							for (var j = 0; j < authData.menuItems.length; j++) {
								if(authData.menuItems[j].state === disabledItems[i]){
									authData.menuItems[j].disabled = true;
								}
							}
						}
					}

					/** @function
					 * @name isUserAuthenticated
					 * @returns {Boolean} wheter if the user is authenticated or not */
					function isUserAuthenticated(){

						authData.token = session.getItem('access-token');
						configuration.token = authData.token;
						return !!authData.token;
					}

					/** @function
					 * @name isNewUser
					 * @returns {Boolean} wheter if this is a new user or not */
					function isNewUser(){
						return authData.newUser;
					}

					/** @function
					 * Notifys the subscribers of Menu Items with the current list of menu items and their availability.
					 * @name getMenuItems */
					function getMenuItems(){
						menuItemsPromise.notify(authData.menuItems);
					}

					/** @function
					 * @name getUserData
					 * @returns {Object} the user information like name and email */
					function getUserData(){
						return authData.user;
					}

					return {
						login:login,
						logout:logout,
						isUserAuthenticated: isUserAuthenticated,
						isNewUser: isNewUser,
						menuItemsPromise: menuItemsPromise.promise,
						getMenuItems:getMenuItems,
						getUserData: getUserData
					};
				}
			])
			.factory('authenticationInterceptor', [
				'$q',
				function authRequestInterceptor($q) {

					/** @function
					 * Inserts authentication data on request header
					 * @name onRequest*/
				    function onRequest(config) {
            			/*jshint camelcase:false*/
            			if(config.url.indexOf('/login') > 0 && config.method === 'POST'){
            				config.headers.username = config.data.email;
            				config.headers.password = config.data.password;
            			}
            			else{
					    	config.headers['access-token'] = configuration.token;
            			}
            			/*jshint camelcase:true*/
			      		return config;
				    }

					/** @function
					 * @name onRequestError*/
				   	function onRequestError(rejection) {
				      	return $q.reject(rejection);
				    }

					/** @function
					 * @name onResponse*/
				    function onResponse(response) {
				    	if(response.config.url.indexOf(configuration.serviceBaseUrl) !== -1){
				    		response = responseTransformer.transformResponse(response);
				    	}
				      	return response;
				    }

					/** @function
					 * @name onResponseError*/
				   	function onResponseError(rejection) {
				    	if(rejection.config.url.indexOf(configuration.serviceBaseUrl) !== -1){
				    		rejection = responseTransformer.transformResponse(rejection);
				    	}
				      	return $q.reject(rejection);
				    }

				    return{
						request: onRequest,
						requestError: onRequestError,
						response: onResponse,
						responseError: onResponseError
					};
			  	}]
			);
	}
);
