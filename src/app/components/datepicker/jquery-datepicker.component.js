define('jquery-datepicker.component',['jquery','angular'],
    function($){
        'use strict';

        angular.module('jqueryDatepickerComponentModule',[])
            .directive('jqueryDatepicker', [
                function() {
                    var directiveDefinition = {
                        restrict: 'C',
                        require: 'ngModel',
                        scope: {
                            showCallback: '&',
                            date: '=',
                            maxDate: '@',
                            minDate: '@'
                        },
                        link: function datepickerLink(scope, element, attrs, ngModelCtrl) {
                            scope.datePattern = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/;
                            scope.maxDate = scope.maxDate || 0;
                            scope.minDate = scope.minDate === '0' ? 0 : scope.minDate || '-100y';

                            function init(){
                                element.datepicker({
                                    showOn: 'both',
                                    buttonImageOnly: true,
                                    buttonImage: '/images/calendar.png',
                                    changeYear: true,
                                    changeMonth: true,
                                    yearRange: 'c-100:c+0',
                                    maxDate: scope.maxDate,
                                    minDate: scope.minDate,
                                    defaultDate: new Date(),
                                    onClose: function(date, inst) {
                                        ngModelCtrl.$setViewValue(date);
                                        scope.date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                                        scope.$apply();
                                    }
                                }).datepicker('setDate', new Date());

                                scope.$watch('date', function(newValue, oldValue) {
                                    if (oldValue !== newValue && typeof (scope.date) === 'string' && !oldValue) {
                                        scope.date = new Date(scope.date);
                                        element.datepicker('setDate', scope.date);
                                        ngModelCtrl.$setViewValue(element.val());
                                    }
                                    ngModelCtrl.$setValidity('pattern', !!scope.datePattern.test(element.val()));
                                });

                                if (scope.showCallback) {
                                    element.next().on('click', function() {
                                        if (angular.isFunction(scope.showCallback)) {
                                            scope.showCallback();
                                        }
                                    });
                                }
                            }

                            if($.ui && $.ui.datepicker){
                                init();
                            }
                            else{
                               require(['jquery-ui.datepicker'], init); 
                            }
                        },
                        controller: function datepickerController(){

                        }
                    };

                    return directiveDefinition;
                }
            ]
        );
    }
);
