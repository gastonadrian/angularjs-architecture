define('bootstrap-modal.component',['jquery','angular'],
    function($){
        'use strict';

        angular.module('bootstrapModalComponentModule',[])
            .directive('bootstrapModal', [
                function() {
                    var definitionObject = {
                        restrict: 'E',
                        replace: true,
                        transclude: true,
                        controllerAs:'modal',
                        bindToController: true,
                        templateUrl: 'app/components/modal/bootstrap-modal.template.html',
                        scope: {
                            modalTitle: '@',
                            dynamicModalTitle: '=',
                            modalContentType: '@',
                            confirmationButton: '@',
                            saveAndCloseButton: '@',
                            showSaveAndCloseButton: '=',
                            cancelButton: '@',
                            keepOpenOnCancel: '=',
                            showModal: '=',
                            modalContentUrl: '=',
                            closeCallback: '&',
                            confirmationCallback: '&',
                            cssClass: '@',
                            suppressX: '@',
                            confirmationButtonDisable: '@'
                        },
                        controller:[
                            function bootstrapModalController() {
                                var that = this;

                                function confirm(obj) {
                                    obj.target.blur(); // ensure :active state is not retained
                                    if (angular.isFunction(that.confirmationCallback)) {
                                        that.confirmationCallback();
                                    }
                                }

                                function cancel(obj) {
                                    obj.target.blur(); // ensure :active state is not retained
                                    that.showModal = !!that.keepOpenOnCancel;

                                    if (angular.isFunction(that.closeCallback)) {
                                        that.closeCallback();
                                    }
                                }

                                function saveAndClose(evt) {
                                    that.confirm(evt);
                                    that.cancel(evt);
                                }

                                function resizeModal(width, height) {
                                    var styleValue = '';
                                    var frame = that.modal.find('.modal-body iframe');
                                    if (width) {

                                        if(frame){
                                            frame.attr('width', width);
                                        }

                                        width += 30;
                                        styleValue += 'width: ' + width + 'px !important;';
                                    }

                                    if (height) {
                                        if (frame){
                                            frame.attr('height', height + 2);
                                        }
                                        height += 77;
                                        styleValue += 'height: ' + height + 'px !important;';
                                    }

                                    that.modal.find('.modal').attr('style', styleValue);
                                }

                                angular.extend(that, {
                                    resizeModal: resizeModal,
                                    confirm:confirm,
                                    cancel:cancel,
                                    saveAndClose:saveAndClose
                                });
                            }
                        ],
                        link: function bootstrapModalLink(scope, element){
                            var that = scope.modal,
                                modalBody = null;

                            function init() {

                                    that.modal = element;
                                    that.backdrop = that.modal.find('.modal-backdrop');
                                    modalBody = that.modal.find('.modal-body');

                                    scope.$watch('modal.dynamicModalTitle', function(newValue) {
                                        if (newValue) {
                                            that.modalTitle = that.dynamicModalTitle;
                                        }
                                    });

                                    scope.$watch('modal.showModal', function(newValue) {

                                        if (newValue) {

                                            // if the content  type is iframe, then set a watch to instantiate the iframe
                                            if (that.modalContentType === 'iframe') {
                                                // if we have to show the modal, then create the content
                                                modalBody.empty();
                                                var iframe = $('<iframe class="frame" height="400" frameborder="0" border="0" src="' + that.modalContentUrl + '"></iframe>');
                                                modalBody.html(iframe);
                                            }

                                            $('body').addClass('modal-open');
                                        } else {
                                            $('body').removeClass('modal-open');
                                        }


                                    });
                                    
                                    scope.$on('$destroy', function() {
                                        cleanup();
                                    });
                            }

                            function cleanup() {
                                element.remove();
                            }

                            init();
                        }
                    };
                    return definitionObject;
                }
            ]);
    }
);