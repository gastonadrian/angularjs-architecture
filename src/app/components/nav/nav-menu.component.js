define('nav-menu.component',['jquery','angular','auth.service'],
	function($){
		'use strict';

		angular.module('navMenuComponentModule',['authServiceModule'])
			.directive('navMenu',[
				function(){
					var definition = {
						restrict: 'E',
						scope:{},
						controllerAs:'navigation',
						bindToController: true,
						templateUrl: 'app/components/nav/nav-menu.template.html',
						replace:true,
						link: function(scope){
							$('main').on('click', function(){
								if(scope.navigation.showMenu){
									scope.$root.$apply(function(){
										scope.navigation.showMenu = false;										
									});
								}
							});							
						},
						controller: [
							'authService',
							'$scope',
							function navMenuController(authService, scope){

								var ctrl = this;
								
								function isVisible(){
									return authService.isUserAuthenticated();
								}

								function onStateChange(){
									ctrl.showMenu = false;
								}

								function init(){
									scope.$root.$on('$stateChangeStart', onStateChange);
							
									// subscribe to changes on menu items
									// the auth service is the responsible of notify when changes
									// on menu items availability happens
									authService.menuItemsPromise
										.finally(null, function onMenuItemsChange(responseData){
											ctrl.items = responseData;
										});

									// trigger get latest modification on menu items
									authService.getMenuItems();

									angular.extend(ctrl, {
										isVisible: isVisible
									});									
								}

								init();
							}
						]
					};

					return definition;
				}
			]);
	}
);