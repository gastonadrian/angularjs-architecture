define('nav-breadcrumb.component',['angular','jquery'],
	function(){
		'use strict';

		angular.module('navBreadcrumbComponentModule',[])
			.directive('navBreadcrumb',[
				function(){
					var definition = {
						restrict: 'E',
						scope:{},
						controllerAs:'breadcrumb',
						bindToController: true,
						templateUrl: 'app/components/breadcrumb/nav-breadcrumb.template.html',
						replace:true,
						controller: [
							'$scope',
							function navBreadcrumbController(scope){

								var ctrl = this;

								var states = [];

								function addState(toState, current){
									current = current && true;

									// append new state
									if(toState.name.indexOf('.') > -1){
										var previousStateName = toState.name.split('.')[0];

										// avoid to have multiple times the same state
										if(!states.length || previousStateName !== states[states.length > 1 ? states.length -1 : 0].name){
											previousStateName = previousStateName.charAt(0).toUpperCase() +  previousStateName.slice(1); 
											states.push({ title: previousStateName, name: toState.name.split('.')[0], current: false });
										}
									}

									states.push({ title: toState.title, name: toState.name, current: current});
								}

								function onStateChange(evt, toState, toParams, fromState){

									if(!states.length){
										addState(fromState);
									}


									for (var i = 0; i < states.length; i++) {
										if(states[i].name === fromState.name || states[i].name === toState.name){

											states[i].current = false;
	
											// remove all states after this one
											if(states[i-1] && toState.name.indexOf('.') !== -1 && states[i-1].name === toState.name.split('.')[0]){
												states.splice(i);
											}
											else{
												states.splice(i+1);
											}

											addState(toState, true);
												
											break;
										}
									}
								}

								function init(){
									scope.$root.$on('$stateChangeStart', onStateChange);

									angular.extend(ctrl, {
										states: states
									});									
								}

								init();
							}
						]
					};

					return definition;
				}
			]);
	}
);