define('alert-modal.component',['angular','jquery'],
    function alertModalModule(){
        'use strict';

        angular.module('alertModalComponentModule',[])
            .directive('alertModal', [
                function alertModalDirective() {
                    var definitionObject = {
                        restrict: 'A',
                        replace: true,
                        transclude: true,
                        scope: {
                            alertTitle: '@',
                            confirmationButton: '@',
                            cancelButton: '@',
                            showAlert: '=',
                            closeCallback: '&',
                            confirmationCallback: '&'
                        },
                        templateUrl: 'app/components/alertModal/alert-modal.template.html',
                        link: function alertModalLink(scope, element) {

                            scope.confirm = function() {
                                if (angular.isFunction(scope.confirmationCallback)) {
                                    scope.confirmationCallback();
                                }
                            };

                            scope.cancel = function() {
                                scope.showAlert = false;

                                if (angular.isFunction(scope.closeCallback)) {
                                    scope.closeCallback();
                                }
                            };

                        }
                    };
                    return definitionObject;
                }
            ]);
    }
);
