(function(){
	'use strict';

	require.config({
		baseUrl: '/',
		paths:{
			'app':'app/app',
			'configuration':'app/config/main.config',
			'angular':'vendor/angular/angular.min',
			'angularRouter':'vendor/angular-ui-router/release/angular-ui-router.min',
			'angularResource':'vendor/angular-resource/angular-resource.min',
			'jquery':'vendor/jquery/dist/jquery.min',
			'login.route':'app/routes/login/login.route',
			'login.controller':'app/routes/login/login.controller',
			'auth.service':'app/services/authentication/authentication',
			'nav-menu.component':'app/components/nav/nav-menu.component',
			'nav-breadcrumb.component':'app/components/breadcrumb/nav-breadcrumb.component',
			'alert-modal.component':'app/components/alertModal/alert-modal.component',
			'jquery-datepicker.component': 'app/components/datepicker/jquery-datepicker.component',
			'jquery-ui.datepicker':'vendor/jquery-ui-1.11.2.custom/jquery-ui',
			'bootstrap-modal.component':'app/components/modal/bootstrap-modal.component',
			'session-storage':'app/helpers/session-storage',
			'response-transformer':'app/helpers/response-transformer'
		},
		shim:{
			'angular':{
				exports: 'jQuery',
				deps:['jquery']
			},
			'angularRouter':{
				deps:['angular']
			},
			'angularResource':{
				deps:['angular']
			}
		}
	});

	require(['app', 'configuration'],
		function(app, config){

			angular.element(document).ready(function() {
			  	angular.bootstrap(document.querySelector('html'), [
			    	config.applicationName
			  	], {
			    	strictDi: true
			  	});
			});

	});
})();
