define('response-transformer',[],
	function(){
		'use strict';

		/* @function
		* Converts the response from the server on a standard way to get data,
		* Useful to avoid receiving hardcoded structures from the server like
				 {
				  'result' : [ {
				    'username' : 'chandra',
				    'password' : 'ZLtdOEj3vRc=',
				    'firstname' : 'chandrashekhar',
				    'lastname' : 'yadav',
				    'email_id' : 'mail@samsung.com',
				    'roles' : [ 'tenant admin' ],
				    'created_time' : '2015-02-03T11:01:52+0000',
				    'created_by' : 'goliathadmin',
				    'updated_time' : '2015-02-03T11:01:52+0000',
				    'last_updated_by' : 'goliathadmin',
				    'is_enabled' : true,
				    'is_active' : true,
				    'doc_type' : 'user',
				    'is_goliath_admin' : false,
				    'tenant_id' : 'b289c46f-5fbf-4a53-acb2-93c30a3a5cdd',
				    'id' : 'c5082287-a3eb-43d6-af8e-d0544419713f'
				  } ],
				  'status' : 'ok',
				  'count' : '1'
				}
		* and being on each call to the server using the 'result' property on a hardcoded way
		* @name convertResponse
		* @param {Object} response - The server response
		* @returns {Object} result { result: Object, status: 'ok', [count], [message] } - The request result */					
		function transformResponse(response) {
			return response;
		}

		return{
			transformResponse: transformResponse
		};
	}
);