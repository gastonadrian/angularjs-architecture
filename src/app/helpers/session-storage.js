define('session-storage',['angular'],
	function(){
		'use strict';

		angular.module('sessionStorageModule',[])
			.factory('sessionStorage', [
				'$q',
				function sessionStorage($q){

					/* @function
					* Creates a cookie entry
					* Based On: http://www.quirksmode.org/js/cookies.html
					* @name createCookie
					* @param {String} name - The item key/name
					* @param {String} value - The item data/value
					* @param {Number} days - The amount of days this will be valid */					
					function createCookie(name,value, days) {
						var expires ='';
						if (days) {
							var date = new Date();
							date.setTime(date.getTime()+(days*60*24*60*1000));
							expires = '; expires='+date.toGMTString();
						}
						document.cookie = name+'='+value+expires+'; path=/';
					}

					/* @function
					* Gets a cookie entry
					* Based On: http://www.quirksmode.org/js/cookies.html
					* @name readCookie
					* @param {String} name - The item name
					* @returns {String} value - The item value */
					function readCookie(name) {
						var nameEQ = name + '=';
						var ca = document.cookie.split(';');
						for(var i=0;i < ca.length;i++) {
							var c = ca[i];
							while (c.charAt(0) ===' ') {
								c = c.substring(1, c.length);
							}
							if (c.indexOf(nameEQ) === 0) {
							 return c.substring(nameEQ.length,c.length);
							}
						}
						return null;
					}

					/* @function
					* Sets a cookie value to empty str`ing
					* Based On: http://www.quirksmode.org/js/cookies.html
					* @name eraseCookie
					* @param {String} name - The item name */
					function eraseCookie(name) {
						createCookie(name,'',-1);
					}

					/* @function
					* Gets a dom storage item
					* @name setItem
					* @param {String} key - The item key
					* @returns {String} value - The item value */
					function setItem(key, name, minutes){
						var defer = $q.defer();

						try{
							if(window.sessionStorage){
								window.sessionStorage.setItem(key, name);
							}
							else{
								// 1 minute = 1 day / (24 hs * 60 minutes)
								var daysValid =  minutes ? ((1 / (24*60)) * minutes) : 1;
								createCookie(key, name, daysValid);
							}
						}
						catch(e){
							defer.reject(e);
						}
					}


					/* @function
					* Gets a dom storage item
					* @name getItem
					* @param {String} key - The item key
					* @returns {String} value - The item value */
					function getItem(key){
						var result;
						if(window.sessionStorage){
							result = window.sessionStorage.getItem(key);
						}
						else{
							result = readCookie(key);
						}
						return result;
					}

					/* @function
					* Removes an item from storage
					* @name removeItem
					* @param {String} key - The item key
					* @returns {String} value - The item value */
					function removeItem(key){
						var defer = $q.defer();

						try{
							if(window.sessionStorage){
								window.sessionStorage.removeItem(key);
							}
							else{
								eraseCookie(key);
							}
							defer.resolve();							
						}
						catch(e){
							defer.reject(e);
						}

						return defer.promise;
					}


					return{
						setItem: setItem,
						getItem: getItem,
						removeItem: removeItem
					};
				}
			]);
	}
);