define('app',
	['angular','angularRouter', 'configuration','login.route','auth.service','nav-menu.component','nav-breadcrumb.component','alert-modal.component', 'jquery-datepicker.component','bootstrap-modal.component'],
	function(ng, angularRouter, config, loginRoute){
		'use strict';

		angular
			.module(config.applicationName,
				['ui.router','authServiceModule','navMenuComponentModule','navBreadcrumbComponentModule','alertModalComponentModule','jqueryDatepickerComponentModule','bootstrapModalComponentModule','loginRouteModule'])
			.run([
				'$rootScope', '$state',
				function($root, $state){
					$root.state = $state;
				}
			])
			.config([
				'$httpProvider',
				'$stateProvider',
				'$urlRouterProvider',
				function($httpProvider, $stateProvider, $urlRouterProvider){

				// authentication provider
			  	$httpProvider.interceptors.push('authenticationInterceptor');

			      $urlRouterProvider
			      	// Match all urls, and redirect if user is no longer authenticated
			      	.when(/.*/, [
			      		'authService',
			      		'$state',
			      		function(authService, state){
			      			// if it is not authenticated redirect to login
			      			if(!authService.isUserAuthenticated()){
										return state.go('login');
			      			}

			      			return false;
			      		}
			      	])
			        .otherwise('/');

        	loginRoute($stateProvider, $urlRouterProvider);
				}
			]);
	}
);
