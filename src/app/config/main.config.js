define('configuration', [],
	function(){
		'use strict';

		var menuItems = [
			{ title: 'Dashboard', icon: 'flaticon-home63', state:'dashboard'  },
			{ title: 'Resources', icon: 'flaticon-concentric', state:'resources'  },
			{ title: 'Domain', icon: 'flaticon-tv28', state:'domain'  },
			{ title: 'Settings', icon: 'flaticon-tools3', state:'settings'  },
			{ title: 'Logout', icon: 'flaticon-tools3', state:'logout'  }
		];

		return {
			applicationName: 'AngularApp',
			serviceBaseUrl: 'http://domain.com/api',
			menuItems: menuItems,
			sessionTimeoutMinutes: 30
		};
	}
);
